# Load provider
provider "docker" {
}

# Docker image part
resource "docker_image" "img" {
  name         = var.name
  keep_locally = var.img.local
}

# Container creation part
resource "docker_container" "task" {
  count = length(var.user)
  name  = var.user[count.index]
  #name  = join("", [var.container.name,count.index])#var.container.name #"${count.index}"
  image = docker_image.img.name
  env   = [join("=", ["${var.container.env}", "${var.env}"])] //[for user in var.user : upper(user)]

  ports {
    internal = var.net.int
    external = var.net.ext + count.index #1234..1236
    ip       = var.net.ip
  }

  volumes {
    volume_name    = var.vol.name
    container_path = var.vol.path
    #read_only = true
  }


  # provisioner "local-exec" {
  #   interpreter = ["curl"]
  #   command = join(":", ["${var.net.ip}", "${var.net.ext}"])
  # }
}

resource "null resource" "exec" {
  provisioner "local-exec" {
    interpreter = ["curl"]
    command     = join(":", ["${var.net.ip}", "${var.net.ext}"])
  }
}