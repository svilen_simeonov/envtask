variable "name" {
  type        = string
  description = "Pick Docker image name: "
} 

variable "env" {
  type        = string
  description = "Set nx_data: "
}

variable "net" {
  type = map

  default = {
    int = 80,
    ext = 1234,
    ip  = "127.0.0.1"
  }
}

variable "img" {
  type = map
  default = {
    name  = ""
    local = true
  }
}


variable "container" {
  type = map
  default = {
    name = "envtask"
    env  = "nx_data"
  }
}

variable "vol" {
  type = map
  default = {
    name = "myVol"
    path = "/usr/share/nginx/html"

  }
}

variable "user" {
  type    = list
  default = ["svilen", "envuser", "otheruser"]
}
